import Chance from 'chance'
import lodash from 'lodash'
import { useEffect, useRef, useState } from 'react'

export default ({ initialMessages }) => {
  const [isGenerating, setIsGenerating] = useState(true)
  const [messages, setMessages] = useState(
    initialMessages || {
      1: [],
      2: [],
      3: [],
    }
  )

  const chance = useRef()
  const timeouts = useRef([])

  /**
   * priority from 1 to 3, 1 = error, 2 = warning, 3 = info
   */
  const generate = () => {
    if (!isGenerating) {
      return
    }
    const text = chance.current.animal()
    const id = chance.current.guid()
    const priority = lodash.random(1, 3)
    const nextInMS = lodash.random(500, 3000)

    setMessages((messages) => ({
      ...messages,
      [priority]: [...messages[priority], { id, text }],
    }))

    timeouts.current.push(
      setTimeout(() => {
        generate()
      }, nextInMS)
    )
  }

  const stop = () => {
    resetTimeouts()
    setIsGenerating(false)
  }

  const start = () => {
    setIsGenerating(true)
  }

  const resetTimeouts = () => {
    timeouts.current.forEach((timeout) => {
      clearTimeout(timeout)
    })
    timeouts.current = []
  }

  useEffect(() => {
    chance.current = new Chance()
  }, [])

  useEffect(() => {
    if (isGenerating) {
      generate()
    }

    return () => {
      resetTimeouts()
    }
  }, [isGenerating])

  return {
    messages,
    setMessages,
    isGenerating,
    start,
    stop,
    generate,
  }
}
