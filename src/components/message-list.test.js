import React from 'react'
import { render, screen, act, within, waitFor } from '@testing-library/react'
import MessageList from './message-list'

const messages = {
  1: [
    { id: 1, text: 'error1' },
    { id: 2, text: 'error2' },
  ],
  2: [
    { id: 1, text: 'warning1' },
    { id: 2, text: 'warning2' },
  ],
  3: [
    { id: 1, text: 'info1' },
    { id: 2, text: 'info2' },
  ],
}

describe('MessageList', () => {
  beforeEach(() => {
    jest.useFakeTimers()
  })
  it('renders title in AppBar', () => {
    render(<MessageList />)
    expect(screen.getByTestId('header')).toHaveTextContent(
      'Nuffsaid.com Coding Challenge'
    )
  })

  it('renders stream of messages', async () => {
    render(<MessageList />)
    expect(await screen.findAllByTestId('message')).toHaveLength(1)

    act(() => {
      jest.advanceTimersByTime(60000)
    })
    expect(
      (await screen.findAllByTestId('message')).length
    ).toBeGreaterThanOrEqual(2)
  })

  it('renders correct # of error messages', async () => {
    render(<MessageList initialMessages={messages} />)

    const errorColumn = screen.getByTestId('error-messages')
    expect(
      within(errorColumn).getAllByTestId('message').length
    ).toBeGreaterThanOrEqual(2)
  })

  it('renders correct # of warning messages', async () => {
    render(<MessageList initialMessages={messages} />)

    const warningColumn = screen.getByTestId('warning-messages')
    expect(
      within(warningColumn).getAllByTestId('message').length
    ).toBeGreaterThanOrEqual(2)
  })

  it('renders correct # of info messages', async () => {
    render(<MessageList initialMessages={messages} />)

    const infoColumn = screen.getByTestId('info-messages')
    expect(
      within(infoColumn).getAllByTestId('message').length
    ).toBeGreaterThanOrEqual(2)
  })

  it('clicking start-stop button starts/stops stream', async () => {
    render(<MessageList />)

    const messageCount = screen.getAllByTestId('message').length

    const startStopButton = screen.getByTestId('start-stop-button')
    await startStopButton.click()

    act(() => {
      jest.advanceTimersByTime(10000)
    })

    expect(screen.getAllByTestId('message').length).toEqual(messageCount)

    await startStopButton.click()

    act(() => {
      jest.advanceTimersByTime(10000)
    })

    expect(screen.getAllByTestId('message').length).toBeGreaterThan(
      messageCount
    )
  })

  it('renders correct message count per column', async () => {
    render(<MessageList />)

    act(() => {
      jest.advanceTimersByTime(60000)
    })

    const startStopButton = screen.getByTestId('start-stop-button')

    await act(async () => {
      await startStopButton.click()
    })

    const errorColumn = screen.getByTestId('error-messages')
    const errorLength = within(errorColumn).getAllByTestId('message').length
    expect(screen.getByTestId('error-count')).toHaveTextContent(
      errorLength.toString()
    )

    const warningColumn = screen.getByTestId('warning-messages')
    const warningLength = within(warningColumn).getAllByTestId('message').length
    expect(screen.getByTestId('warning-count')).toHaveTextContent(
      warningLength.toString()
    )

    const infoColumn = screen.getByTestId('info-messages')
    const infoLength = within(infoColumn).getAllByTestId('message').length
    expect(screen.getByTestId('info-count')).toHaveTextContent(
      infoLength.toString()
    )
  })

  it('clicking clear button clears messages', async () => {
    render(<MessageList />)

    act(() => {
      jest.advanceTimersByTime(60000)
    })

    const clearButton = screen.getByTestId('clear-button')
    const startStopButton = screen.getByTestId('start-stop-button')

    await act(async () => {
      await startStopButton.click()
    })

    await act(async () => {
      await clearButton.click()
    })

    expect(screen.queryAllByTestId('message')).toEqual([])
  })

  it('snackbar shows error message when present, disappears in 2 seconds', async () => {
    render(<MessageList initialMessages={messages} />)

    expect(screen.queryByTestId('snackbar-message')).toBeTruthy()

    waitFor(() => expect(screen.queryByTestId('snackbar-message')).toBeNull(), {
      timeout: 2000,
    })
  })

  it('snackbar shows error message when present, click x to close', async () => {
    render(<MessageList initialMessages={messages} />)

    const snackbarMessage = await screen.findByTestId('snackbar-message')
    const snackbarMessageCloseButton = await within(
      snackbarMessage
    ).findByTitle('Close')

    expect(snackbarMessage).toBeTruthy()
    expect(snackbarMessageCloseButton).toBeTruthy()

    await act(async () => {
      await snackbarMessageCloseButton.click()
    })

    waitFor(() => expect(screen.queryByTestId('snackbar-message')).toBeNull())
  })

  it('clicking x icon on message removes item', async () => {
    render(<MessageList initialMessages={messages} />)

    act(() => {
      jest.advanceTimersByTime(60000)
    })

    const startStopButton = screen.getByTestId('start-stop-button')

    await act(async () => {
      await startStopButton.click()
    })

    const allMessages = screen.getAllByTestId('message')
    const closeButton = within(allMessages[0]).getByTitle('Close')
    const messagesLength = allMessages.length

    await act(async () => {
      await closeButton.click()
    })

    waitFor(() =>
      expect(screen.getAllByTestId('message')).toHaveLength(messagesLength - 1)
    )
  })
})
