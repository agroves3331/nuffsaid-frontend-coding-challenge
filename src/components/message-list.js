import React, { useCallback, useEffect, useState } from 'react'
import {
  AppBar,
  Box,
  Button,
  Grid,
  Snackbar,
  Toolbar,
  Typography,
  makeStyles,
} from '@material-ui/core'
import { Alert } from '@material-ui/lab'

import useApi from '../hooks/useApi'

const ALERT_COLOR_MAP = {
  1: 'error',
  2: 'warning',
  3: 'info',
}

const useStyles = makeStyles((theme) => ({
  alert: {
    marginBottom: theme.spacing(1),
  },
  table: {
    marginTop: theme.spacing(8),
  },
  buttonRow: {
    '& > *': {
      marginRight: theme.spacing(1),
      marginTop: theme.spacing(2),
    },
  },
}))

const MessageList = ({ initialMessages }) => {
  const [snackbarOpen, setSnackbarOpen] = useState(false)
  const { messages, setMessages, isGenerating, start, stop } = useApi({
    initialMessages,
  })
  const classes = useStyles()

  useEffect(() => {
    start()
  }, [])

  useEffect(() => {
    if (messages[1].length) {
      setSnackbarOpen(true)
    }
  }, [messages[1]])

  const handleStartStopClick = useCallback(() => {
    if (isGenerating) {
      stop()
    } else {
      start()
    }
  }, [isGenerating])

  const handleSnackbarClose = useCallback(() => {
    setSnackbarOpen(false)
  }, [setSnackbarOpen])

  const handleClearClick = useCallback(() => {
    setMessages({
      1: [],
      2: [],
      3: [],
    })
  }, [setSnackbarOpen])

  return (
    <>
      <AppBar
        color="inherit"
        position="sticky"
        data-testid="header"
        variant="outlined"
      >
        <Toolbar>
          <Typography variant="h6">Nuffsaid.com Coding Challenge</Typography>
        </Toolbar>
      </AppBar>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={snackbarOpen}
        autoHideDuration={2000}
        onClose={handleSnackbarClose}
      >
        <Alert
          onClose={handleSnackbarClose}
          severity="error"
          variant="filled"
          data-testid="snackbar-message"
        >
          {messages[1][messages[1].length - 1] &&
            messages[1][messages[1].length - 1].text}
        </Alert>
      </Snackbar>
      <Box className={classes.buttonRow} display="flex" justifyContent="center">
        <Button
          onClick={handleStartStopClick}
          variant="contained"
          color="primary"
          data-testid="start-stop-button"
        >
          {isGenerating ? 'Stop' : 'Start'} Messages
        </Button>
        <Button
          onClick={handleClearClick}
          variant="contained"
          color="primary"
          data-testid="clear-button"
        >
          Clear
        </Button>
      </Box>
      <Table messages={messages} setMessages={setMessages} />
    </>
  )
}

const Table = ({ messages, setMessages }) => {
  const classes = useStyles()

  const handleRemoveAlert = (priority, index) => {
    setMessages((messages) => ({
      ...messages,
      [priority]: messages[priority].filter((_, i) => i !== index),
    }))
  }

  const renderMessages = (priority) => {
    return messages[priority].map((message, index) => (
      <Alert
        key={`${message.id}`}
        severity={ALERT_COLOR_MAP[priority]}
        variant="filled"
        mb={2}
        className={classes.alert}
        onClose={() => handleRemoveAlert(priority, index)}
        data-testid="message"
      >
        {message.text}
      </Alert>
    ))
  }

  return (
    <Box p={4}>
      <Grid container spacing={2} className={classes.table}>
        <Grid item xs={12} sm={4} data-testid="error-messages">
          <Typography variant="h6">Error Type 2</Typography>
          <Typography variant="subtitle1">
            Count <Box data-testid="error-count">{messages[1].length}</Box>
          </Typography>
          {renderMessages(1)}
        </Grid>
        <Grid item xs={12} sm={4} data-testid="warning-messages">
          <Typography variant="h6">Warning Type 2</Typography>
          <Typography variant="subtitle1">
            Count <Box data-testid="warning-count">{messages[2].length}</Box>
          </Typography>
          {renderMessages(2)}
        </Grid>
        <Grid item xs={12} sm={4} data-testid="info-messages">
          <Typography variant="h6">Info Type 2</Typography>
          <Typography variant="subtitle1">
            Count <Box data-testid="info-count">{messages[3].length}</Box>
          </Typography>

          {renderMessages(3)}
        </Grid>
      </Grid>
    </Box>
  )
}

export default MessageList
