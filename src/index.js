import React from 'react'
import ReactDOM from 'react-dom'
import MessageList from './components/message-list'
import { MuiThemeProvider, CssBaseline } from '@material-ui/core'
import { theme } from './theme'

const NewApp = require('./components/message-list').default

function renderApp(App) {
  ReactDOM.render(
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <App />
    </MuiThemeProvider>,
    document.getElementById('root')
  )
}

renderApp(MessageList)

if (module.hot) {
  module.hot.accept('./components/message-list', () => {
    renderApp(NewApp)
  })
}
