import { createTheme } from '@material-ui/core'

export const theme = createTheme({
  palette: {
    error: {
      main: '#F56236',
    },
    warning: {
      main: '#FCE788',
    },
    info: {
      main: '#88FCA3',
    },
  },
  overrides: {
    MuiAlert: {
      message: {
        color: 'black',
      },
      icon: {
        color: 'black',
      },
      action: {
        color: 'black',
      },
    },
  },
})
